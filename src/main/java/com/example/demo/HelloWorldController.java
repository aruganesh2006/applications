package com.example.demo;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	   /* @RequestMapping("/hello")
	    public void hello() {
		 	System.out.println("hello");	       
	    }*/
	    
	    @RequestMapping("/hello")
	    public String hello(Model model, @RequestParam(value="name", defaultValue="World") String name) {
		 	System.out.println("hello");
	        model.addAttribute("name", name);
	        return "hello";
	    }
}
