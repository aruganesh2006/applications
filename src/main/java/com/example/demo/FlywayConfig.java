package com.example.demo;

import org.flywaydb.core.api.callback.Callback;
import org.flywaydb.core.api.callback.Context;
import org.flywaydb.core.api.callback.Event;
import org.flywaydb.core.api.configuration.ConfigurationAware;
import org.flywaydb.core.api.configuration.FlywayConfiguration;
import org.springframework.stereotype.Service;


public class FlywayConfig implements Callback {

    /*@Autowired
    private RedisResourceAccessRepository redisResourceAccessRepository;
*/
    /*
     * @Override public void afterEachMigrate(Connection connection, MigrationInfo
     * info) { System.out.println("flyway Implementation"); System.out.println("1."
     * + info.getScript());
     *
     * System.out.println("2." + info.getDescription()); if
     * (info.getState().equals(MigrationState.SUCCESS)) {
     * System.out.println("3. Success"); // TODO }
     *
     *
     * String key = "*statusType*";
     * System.out.println(redisResourceAccessRepository);
     *
     *
     * ArrayList<QtiStatus> statusList = (ArrayList<QtiStatus>)
     * redisResourceAccessRepository.find(key); System.out.println("statusList--" +
     * statusList.size());
     *
     * }
     */
	/*FlywayConfig(){
		System.out.println("FlywayConfig object created");
	}*/
	
    @Override
    public boolean supports(Event event, Context context) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean canHandleInTransaction(Event event, Context context) {
    	System.out.println("inside canHandleInTransaction method");         
        return false;
    }

    @Override
    public void handle(Event event, Context context) {
        // TODO Auto-generated method stub
    	System.out.println("inside handle method");
        if (event.name().equals(Event.AFTER_EACH_MIGRATE)) {
            System.out.println("AFTER_EACH_MIGRATE");
        }
        System.out.println("sql statement--" + context.getStatement().getSql());
        //System.out.println(redisResourceAccessRepository);
    }
}
